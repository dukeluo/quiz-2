### Story 1

- 给定一个Rob用户`{"firstName": null, "lastName": null}`，当使用`post`方法对`/api/staffs`访问时，应该得到状态码400

- 给定一个Rob用户`{"firstName": "Rob", "lastName": "Hall"}`，当使用`post`方法对`/api/staffs`访问时，应该成功创建`staff`，得到状态码201，并且location的值为`/api/staffs/{staffId}`

- 给定存有id为1的Rob用户的情况，当使用`get`方法对`/api/staffs/1`访问时，应该成功返回`staff`，得到状态码200，并且content的内容为此用户的信息，包括`id`，`firstName`，`lastName`

- 当使用`get`方法对`/api/staffs/1`访问时，应该得到状态码404

- 给定存有id为1和id为2的Rob用户的情况，当使用`get`方法对`/api/staffs`访问时，应该成功返回content的内容为一个`staff`数组，包括`id`，`firstName`，`lastName`，并且按照id排序

- 当使用`get`方法对`/api/staffs`访问时，应该成功返回content的内容为一个空数组

### Story 2

- 给定存有id为1的Rob用户的情况，当使用`put`方法对`/api/staffs/1/timezone`访问时，请求的content内容为`{"zoneId": "Asia/Chongqing"}`，得到状态码200
- 给定存有id为1的Rob用户的情况，当使用`put`方法对`/api/staffs/1/timezone`访问时，请求的content内容为空，得到状态码400
- 给定存有id为1的Rob用户的情况，当使用`put`方法对`/api/staffs/1/timezone`访问时，请求的content内容为`{"zoneId": "Asia/Home"}`，得到状态码400
- 给定存有id为1的Rob用户的情况，其存在timeId信息，当使用`get`方法对`/api/staffs/1`访问时，返回content的内容为此用户的信息，包括`id`，`firstName`，`lastName`，`zoneId`
- 给定存有id为1的Rob用户的情况，其不存在timeId信息，当使用`get`方法对`/api/staffs/1`访问时，返回content的内容为此用户的信息，包括`id`，`firstName`，`lastName`，`zoneId`，其中`zoneId`为null

### Story 3

- 当使用`get`方法对`/api/timezones`访问时，返回content的内容为所有按升序排序的zoneId数组

### Story 4

- 给定一个id为1的Rob用户`{"firstName": "Rob", "lastName": "Hall"}`，当使用`post`方法对`/api/staffs/1/reservations`访问时，请求的content为`{"username": "Sofia","companyName": "ThoughtWorks","zoneId": "Africa/Nairobi","startTime": "2019-08-20T11:46:00+03:00","duration": "PT1H"}`，应该成功创建添加预约，得到状态码201，并且location的值为`/api/staffs/1/reservations`
- 给定一个id为`1`，zoneId为`Asia/Chongqing`的Rob用户`{"firstName": "Rob", "lastName": "Hall"}`，并且有两个预约`{"username": "Jack","companyName": "Alibaba","zoneId": "Asia/Chongqing","startTime": "2019-08-21T11:46:00+03:00","duration": "PT2H"}`和`{"username": "Sofia","companyName": "ThoughtWorks","zoneId": "Africa/Nairobi","startTime": "2019-08-20T11:46:00+03:00","duration": "PT1H"}`，当使用`get`方法对`/api/staffs/1/reservations`访问时，应该得到该Rob用户的所有预约信息，返回的content为一个列表，包含预约的各个信息，并按照`startTime`排序
- 给定一个id为1，zoneId为`null`的Rob用户`{"firstName": "Rob", "lastName": "Hall"}`，当使用`post`方法对`/api/staffs/1/reservations`访问时，请求的content为`{"username": "Sofia","companyName": "ThoughtWorks","zoneId": "Africa/Nairobi","startTime": "2019-08-20T11:46:00+03:00","duration": "PT1H"}`，添加预约失败，得到状态码409，并且content的值为`{"message": "Rob Hall is not qualified."}`
- 给定一个id为1，zoneId为`Asia/Chongqing`的Rob用户`{"firstName": "Rob", "lastName": "Hall"}`，当使用`post`方法对`/api/staffs/1/reservations`访问时，请求的content为`{"username": "Sofia","companyName": "ThoughtWorks","zoneId": "Africa/Nairobi","startTime": "2019-10-14T11:46:00+03:00","duration": "PT1H"}`，添加预约失败，得到状态码400，并且content的值为`{"message": "Invalid time: the application is too close from now. The interval should be greater than 48 hours."}`
- 给定一个id为1，zoneId为`Asia/Chongqing`的Rob用户`{"firstName": "Rob", "lastName": "Hall"}`，当使用`post`方法对`/api/staffs/1/reservations`访问时，请求的content为`{"username": "Sofia","companyName": "ThoughtWorks","zoneId": "Africa/Nairobi","startTime": "2019-10-14T11:46:00+03:00","duration": "PT1H"}`，添加预约失败，得到状态码400，并且content的值为`{"message": "You know, our staff has their own life."}`
- 给定一个id为1，zoneId为`Asia/Chongqing`的Rob用户`{"firstName": "Rob", "lastName": "Hall"}`，当使用`post`方法对`/api/staffs/1/reservations`访问时，请求的content为`{"username": "Sofia","companyName": "ThoughtWorks","zoneId": "Africa/Nairobi","startTime": "2019-12-20T11:46:00+03:00","duration": "PT4H"}`，添加预约失败，得到状态码400，并且content的值为`{"message": "The application duration should be within 1-3 hours."}`





