package com.twuc.webApp.contract;

import com.twuc.webApp.domain.Reservation;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.validation.constraints.Pattern;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.zone.ZoneRulesProvider;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;

public class AddReservationRequest {

    @NotNull
    @Size(min = 1, max = 128)
    private String username;

    @NotNull
    @Size(min = 1, max = 64)
    private String companyName;

    @NotNull
    private String zoneId;

    @NotNull
    private String startTime;

    @NotNull
    @Pattern(regexp="PT[1-3]H", message = "The application duration should be within 1-3 hours.")
    private String duration;

    public AddReservationRequest(@NotNull @Size(min = 1, max = 128) String username,
                                 @NotNull @Size(min = 1, max = 64) String companyName,
                                 @NotNull String zoneId,
                                 @NotNull String startTime,
                                 @NotNull String duration) {
        this.username = username;
        this.companyName = companyName;
        this.zoneId = zoneId;
        this.startTime = startTime;
        this.duration = duration;

        validZoneId();
    }

    public AddReservationRequest() {
    }

    public String getUsername() {
        return username;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getDuration() {
        return duration;
    }

    public String getZoneId() {
        return zoneId;
    }

    public Reservation toReservation() {
        return new Reservation(username, companyName, duration, startTime, zoneId);
    }

    private void validZoneId() {
        Set<String> allZoneIds = ZoneRulesProvider.getAvailableZoneIds();

        if (!allZoneIds.contains(zoneId)) {
            throw new IllegalArgumentException();
        }
    }

//    private void validDuration() {
//        String pattern = "PT[1-3]H";
//
////        if (!duration.matches(pattern)) {
//            System.out.println("_________________________");
//            throw new IllegalArgumentException("The application duration should be within 1-3 hours.");
////        }
////        System.out.println("+++++++++++++++++++++");
//    }

    private void validStartTime() {
        SimpleDateFormat plusFormat = new SimpleDateFormat("YYYY-MM-DD'T'hh:mm:ss+hh:mm");
        SimpleDateFormat minusFormat = new SimpleDateFormat("YYYY-MM-DD'T'hh:mm:ss-hh:mm");
        List<SimpleDateFormat> formats = Arrays.asList(plusFormat, minusFormat);


        formats.forEach((format) -> {
            ParsePosition pos = new ParsePosition(0);

            // If you set lenient to false, ranges will be checked, e.g.
            // seconds must be in the range of 0..59 inclusive.
            format.setLenient(false);
            format.parse(startTime, pos);// No declared exception, no need try-catch

            if (pos.getErrorIndex() >= 0 || pos.getIndex() != startTime.length()) {
                throw new IllegalArgumentException();
            }
        });
    }
}
