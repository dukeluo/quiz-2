package com.twuc.webApp.contract;

import com.twuc.webApp.domain.Staff;

import java.util.Objects;

public class GetStaffResponse {

    private Long id;
    private String firstName;
    private String lastName;
    private String timeId;

    public GetStaffResponse(Staff staff) {
        Objects.requireNonNull(staff);

        this.id = staff.getId();
        this.firstName = staff.getFirstName();
        this.lastName = staff.getLastName();
        this.timeId = staff.getZoneId();
    }

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getTimeId() {
        return timeId;
    }
}
