package com.twuc.webApp.contract;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.twuc.webApp.domain.Reservation;
import com.twuc.webApp.domain.Staff;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class GetReservationResponse {

    private String username;
    private String companyName;
    private String duration;
    private Map<String, StartTime> startTimeMap = new HashMap<String, StartTime>();

    public GetReservationResponse(Reservation reservation, Staff staff) {
        Objects.requireNonNull(reservation);
        Objects.requireNonNull(staff);

        this.username = reservation.getUsername();
        this.companyName = reservation.getCompanyName();
        this.duration = reservation.getDuration();

        addClientStartTime(reservation.getStartTime(), reservation.getZoneId());
        addStaffStartTime(reservation.getStartTime(), staff);
    }

    private void addClientStartTime(String startTime, String zoneId) {
        startTimeMap.put("client",
                new StartTime(instantTextToZonedDateTimeText(startTime, zoneId), zoneId));
    }

    private void addStaffStartTime(String startTime, Staff staff) {
        String staffZoneId = staff.getZoneId();

        startTimeMap.put("staff",
                new StartTime(instantTextToZonedDateTimeText(startTime, staffZoneId), staffZoneId));
    }

    private String instantTextToZonedDateTimeText(String instant, String zoneId) {
        Instant now = Instant.parse(instant);
        ZonedDateTime zonedDateTime = now.atZone(ZoneId.of(zoneId));
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");

        return zonedDateTime.format(formatter) + zonedDateTime.getOffset().toString();
    }

    public String getUsername() {
        return username;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getDuration() {
        return duration;
    }

    @JsonProperty(value = "startTime")
    public Map<String, StartTime> getStartTimeMap() {
        return startTimeMap;
    }

}

class StartTime {
    private String startTime;
    private String zoneId;

    StartTime(String startTime, String zoneId) {
        this.startTime = startTime;
        this.zoneId = zoneId;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getZoneId() {
        return zoneId;
    }
}
