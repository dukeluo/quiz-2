package com.twuc.webApp.contract;

import javax.validation.constraints.NotNull;

public class AddTimeZoneRequest {

    @NotNull
    private String zoneId;

    public AddTimeZoneRequest(@NotNull String zoneId) {
        this.zoneId = zoneId;
    }

    public AddTimeZoneRequest() {
    }

    public String getZoneId() {
        return zoneId;
    }
}
