package com.twuc.webApp.domain;

import javax.persistence.*;
import java.time.Instant;
import java.time.ZonedDateTime;

@Entity
public class Reservation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "username")
    private String username;

    @Column(name = "company_name")
    private String companyName;

    @Column(name = "duration")
    private String duration;

    @Column(name = "start_time")
    private String startTime;

    @Column(name = "zone_id")
    private String zoneId;

    @ManyToOne
    private Staff staff;

    public Reservation(String username, String companyName, String duration, String startTime, String zoneId) {
        ZonedDateTime clientTime = ZonedDateTime.parse(startTime + "[" + zoneId + "]");
        Instant now = clientTime.toInstant();

        this.username = username;
        this.companyName = companyName;
        this.duration = duration;
        this.startTime = now.toString();
        this.zoneId = zoneId;
    }

    public Reservation() {
    }

    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getDuration() {
        return duration;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getZoneId() {
        return zoneId;
    }

    public Staff getStaff() {
        return staff;
    }

    public void setStaff(Staff staff) {
        this.staff = staff;
    }
}
