package com.twuc.webApp.domain;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface StaffRepository extends JpaRepository<Staff, Long> {
    Optional<List<Staff>> findAllByOrderByIdAsc();
}
