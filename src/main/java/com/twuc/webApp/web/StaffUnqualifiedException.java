package com.twuc.webApp.web;

import com.twuc.webApp.domain.Staff;

class StaffUnqualifiedException extends RuntimeException {
    private Staff staff;

    StaffUnqualifiedException(Staff staff) {
        this.staff = staff;
    }

    @Override
    public String getMessage() {
        return String.format("%s %s is not qualified.", staff.getFirstName(), staff.getLastName());
    }
}
