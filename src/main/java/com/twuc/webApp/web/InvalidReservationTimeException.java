package com.twuc.webApp.web;

public class InvalidReservationTimeException extends IllegalArgumentException {
    @Override
    public String getMessage() {
        return "Invalid time: the application is too close from now. The interval should be greater than 48 hours.";
    }
}
