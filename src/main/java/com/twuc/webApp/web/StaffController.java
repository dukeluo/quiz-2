package com.twuc.webApp.web;

import com.twuc.webApp.contract.*;
import com.twuc.webApp.domain.Reservation;
import com.twuc.webApp.domain.ReservationRepository;
import com.twuc.webApp.domain.Staff;
import com.twuc.webApp.domain.StaffRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.time.zone.ZoneRulesProvider;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api")
public class StaffController {

    @Autowired
    private StaffRepository staffRepository;

    @Autowired
    private ReservationRepository reservationRepository;

    @PostMapping(value = "/staffs")
    public ResponseEntity<Void> addStaff(@Valid @RequestBody CreateStaffRequest request) {
        Staff staff = request.toStaff();

        staffRepository.save(staff);
        return ResponseEntity.status(HttpStatus.CREATED)
                .header("Location", String.format("/api/staffs/%d", staff.getId()))
                .body(null);
    }

    @GetMapping(value = "/staffs/{id}")
    public ResponseEntity<GetStaffResponse> getStaff(@PathVariable(name = "id") Long id) {
        Staff staff = staffRepository.findById(id).get();

        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(new GetStaffResponse(staff));
    }

    @GetMapping(value = "/staffs")
    public ResponseEntity<List<GetStaffResponse>> getStaffs() {
        List<Staff> staffs = staffRepository.findAllByOrderByIdAsc().orElse(new ArrayList<Staff>());

        List<GetStaffResponse> staffResponses = staffs.stream().map(GetStaffResponse::new).collect(Collectors.toList());
        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(staffResponses);
    }

    @PutMapping(value = "/staffs/{staffId}/timezone")
    public ResponseEntity<Void> addTimezoneForStaff(@Valid @RequestBody AddTimeZoneRequest request,
                                                    @PathVariable(name = "staffId") Long id){
        Set<String> allZoneIds = ZoneRulesProvider.getAvailableZoneIds();
        String requestZoneId = request.getZoneId();

        if(!allZoneIds.contains(requestZoneId)) {
            throw new IllegalArgumentException();
        }

        Staff staff = staffRepository.findById(id).get();

        staff.setZoneId(requestZoneId);
        staffRepository.save(staff);
        return ResponseEntity.status(HttpStatus.OK)
                .body(null);
    }

    @GetMapping(value ="/timezones")
    public ResponseEntity<List<String>> getTimeZones() {
        List<String> allZoneIds = new ArrayList<>(ZoneRulesProvider.getAvailableZoneIds());

        Collections.sort(allZoneIds);

        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(allZoneIds);
    }

    @PostMapping(value = "/staffs/{staffId}/reservations")
    public ResponseEntity<Void> addReservation(@Valid @RequestBody AddReservationRequest request,
                                               @PathVariable(name = "staffId") Long id) {
        Staff staff = staffRepository.findById(id).get();
        Reservation reservation = request.toReservation();
        Instant reservationTime = Instant.parse(reservation.getStartTime());
        Instant now = Instant.now();

        if (staff.getZoneId() == null) {
            throw new StaffUnqualifiedException(staff);
        }
        if (!reservationTime.isAfter(now.plus(2, ChronoUnit.DAYS))) {
            throw new InvalidReservationTimeException();
        }

        int hour = ZonedDateTime.ofInstant(reservationTime, ZoneId.of(staff.getZoneId())).getHour();

        if (hour < 9 || hour >= 17) {
            throw new OutOfWorkTimeException();
        }

        reservation.setStaff(staff);
        staff.getReservations().add(reservation);
        staffRepository.save(staff);

        return ResponseEntity.status(HttpStatus.CREATED)
                .header("Location", String.format("/api/staffs/%d/reservations", id))
                .body(null);
    }

    @GetMapping(value = "/staffs/{staffId}/reservations")
    public ResponseEntity<List<GetReservationResponse>> getAllReservations(@PathVariable(name = "staffId") Long id) {
        Staff staff = staffRepository.findById(id).get();

        List<Reservation> reservations = reservationRepository.findReservationsByStaffIdOrderByStartTime(id)
                .orElse(new ArrayList<Reservation>());
        List<GetReservationResponse> reservationResponses = reservations.stream()
                .map((reservation) -> new GetReservationResponse(reservation, staff))
                .collect(Collectors.toList());

        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(reservationResponses);
    }

    @ExceptionHandler(value = {MethodArgumentNotValidException.class})
    public ResponseEntity<ExceptionResponse> validationHandler(MethodArgumentNotValidException exception) {
        String message = exception.getBindingResult().getAllErrors().get(0).getDefaultMessage();

        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new ExceptionResponse(new Exception(message)));
    }

    @ExceptionHandler(value = {IllegalArgumentException.class})
    public ResponseEntity<ExceptionResponse> illegalArgumentExceptionHandler(IllegalArgumentException exception) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new ExceptionResponse(exception));
    }

    @ExceptionHandler(value = {NoSuchElementException.class})
    public ResponseEntity<String> noSuchElementExceptionHandler(NoSuchElementException exception) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body("No such staff");
    }

    @ExceptionHandler(value = {StaffUnqualifiedException.class, OutOfWorkTimeException.class})
    public ResponseEntity<ExceptionResponse> staffUnqualifiedExceptionHandler(RuntimeException exception) {
        return ResponseEntity.status(HttpStatus.CONFLICT)
                .body(new ExceptionResponse(exception));
    }
}
