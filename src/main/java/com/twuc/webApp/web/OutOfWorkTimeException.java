package com.twuc.webApp.web;

public class OutOfWorkTimeException extends RuntimeException {
    @Override
    public String getMessage() {
        return "You know, our staff has their own life.";
    }
}
