CREATE TABLE IF NOT EXISTS reservation (
    `id` BIGINT AUTO_INCREMENT PRIMARY KEY,
    `company_name` VARCHAR(64) NOT NULL,
    `duration` VARCHAR(64) NOT NULL,
    `start_time` VARCHAR(64) NOT NULL,
    `username` VARCHAR(64) NOT NULL,
    `zone_id` VARCHAR(64) NOT NULL,
    `staff_id` BIGINT,
    FOREIGN KEY (`staff_id`) REFERENCES staff(`id`)
);