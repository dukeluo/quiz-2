package com.twuc.webApp;

import com.twuc.webApp.contract.AddReservationRequest;
import com.twuc.webApp.domain.Reservation;
import com.twuc.webApp.domain.ReservationRepository;
import com.twuc.webApp.domain.Staff;
import com.twuc.webApp.domain.StaffRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.zone.ZoneRulesProvider;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

class StaffControllerTest extends ApiTestBase {

    @Autowired
    private StaffRepository staffRepository;

    @Test
    void should_get_400_when_the_staff_first_name_and_last_name_is_null() throws Exception {
        Staff staff = new Staff(null, null);

        mockMvc.perform(MockMvcRequestBuilders.post("/api/staffs")
                .contentType("application/json;charset=UTF-8")
                .content(serialize(staff)))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    void should_add_a_staff_and_get_status_201_and_location() throws Exception {
        Staff staff = new Staff("Rob", "Hall");

        mockMvc.perform(MockMvcRequestBuilders.post("/api/staffs")
                .contentType("application/json;charset=UTF-8")
                .content(serialize(staff)))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.header().string("Location", "/api/staffs/1"));

        Staff savedStaff = staffRepository.findById(1L).get();
        assertEquals(staff.getFirstName(), savedStaff.getFirstName());
        assertEquals(staff.getLastName(), savedStaff.getLastName());
    }

    @Test
    void should_get_a_staff() throws Exception {
        Staff staff = new Staff("Rob", "Hall");

        staffRepository.save(staff);
        staffRepository.flush();

        mockMvc.perform(MockMvcRequestBuilders.get("/api/staffs/1"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.firstName").value(staff.getFirstName()))
                .andExpect(jsonPath("$.lastName").value(staff.getLastName()));
    }

    @Test
    void should_get_404() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/staffs/1"))
                .andExpect(MockMvcResultMatchers.status().is(404));
    }

    @Test
    void should_get_all_staffs_ordered_by_id() throws Exception {
        Staff staff1 = new Staff("Rob", "Hall");
        Staff staff2 = new Staff("Jack", "Ma");
        Staff staff3 = new Staff("Robin", "Li");

        staffRepository.saveAll(Arrays.asList(staff1, staff3, staff2));
        staffRepository.flush();

        mockMvc.perform(MockMvcRequestBuilders.get("/api/staffs"))
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[1].id").value(2))
                .andExpect(jsonPath("$[2].id").value(3))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void should_return_a_empty_array() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/staffs"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("[]"));
    }

    @Test
    void should_add_time_zone_for_a_staff() throws Exception {
        Staff staff = new Staff("Rob", "Hall");
        String content = "{\n" +
                "\"zoneId\":\"Asia/Chongqing\"\n" +
                "}";

        staffRepository.save(staff);
        staffRepository.flush();

        mockMvc.perform(MockMvcRequestBuilders.put("/api/staffs/1/timezone")
                .content(content)
                .contentType("application/json;charset=UTF-8"))
                .andExpect(MockMvcResultMatchers.status().is(200));

        Staff savedStaff = staffRepository.findById(1L).get();

        assertEquals("Asia/Chongqing", savedStaff.getZoneId());
    }

    @Test
    void should_get_status_400_when_not_provide_time_zone_id() throws Exception {
        Staff staff = new Staff("Rob", "Hall");

        staffRepository.save(staff);
        staffRepository.flush();

        mockMvc.perform(MockMvcRequestBuilders.put("/api/staffs/1/timezone")
                .content("")
                .contentType("application/json;charset=UTF-8"))
                .andExpect(MockMvcResultMatchers.status().is(400));

    }

    @Test
    void should_get_400_when_time_zone_is_wrong() throws Exception {
        Staff staff = new Staff("Rob", "Hall");
        String content = "{\n" +
                "\"zoneId\":\"Asia/Home\"\n" +
                "}";

        staffRepository.save(staff);
        staffRepository.flush();

        mockMvc.perform(MockMvcRequestBuilders.put("/api/staffs/1/timezone")
                .content(content)
                .contentType("application/json;charset=UTF-8"))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    void should_get_a_staff_with_time_id() throws Exception {
        Staff staff = new Staff("Rob", "Hall");

        staff.setZoneId("Asia/Chongqing");
        staffRepository.save(staff);
        staffRepository.flush();

        mockMvc.perform(MockMvcRequestBuilders.get("/api/staffs/1"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.firstName").value(staff.getFirstName()))
                .andExpect(jsonPath("$.lastName").value(staff.getLastName()))
                .andExpect(jsonPath("$.timeId").value(staff.getZoneId()));
    }

    @Test
    void should_get_a_staff_with_time_id_null() throws Exception {
        Staff staff = new Staff("Rob", "Hall");

        staffRepository.save(staff);
        staffRepository.flush();

        mockMvc.perform(MockMvcRequestBuilders.get("/api/staffs/1"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.firstName").value(staff.getFirstName()))
                .andExpect(jsonPath("$.lastName").value(staff.getLastName()))
                .andExpect(jsonPath("$.timeId").value(staff.getZoneId()));
    }

    @Test
    void should_get_all_time_zone_ids() throws Exception {
        List<String> allZoneIds = new ArrayList<>(ZoneRulesProvider.getAvailableZoneIds());

        Collections.sort(allZoneIds);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/timezones"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string(serialize(allZoneIds)));
    }

    @Test
    void should_get_status_201_and_location_when_add_a_reservation_successfully() throws Exception {
        Staff staff = new Staff("Rob", "Hall");
        String content = "{\n" +
                "  \"username\": \"Sofia\",\n" +
                "  \"companyName\": \"ThoughtWorks\",\n" +
                "  \"zoneId\": \"Africa/Nairobi\",\n" +
                "  \"startTime\": \"2019-12-20T11:46:00+03:00\",\n" +
                "  \"duration\": \"PT1H\"\n" +
                "}\n";

        staff.setZoneId("Asia/Chongqing");
        staffRepository.save(staff);
        staffRepository.flush();

        mockMvc.perform(MockMvcRequestBuilders.post("/api/staffs/1/reservations")
                    .contentType("application/json")
                    .content(content))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.header().string("Location", "/api/staffs/1/reservations"));
    }

    @Test
    void should_get_all_reservations() throws Exception {
        Staff staff = new Staff("Rob", "Hall");
        Reservation reservation = new Reservation("Jack", "Alibaba", "PT2H", "2019-08-21T11:46:00+08:00", "Asia/Chongqing");
        Reservation anotherReservation = new Reservation("Sofia", "ThoughtWorks", "PT1H", "2019-08-20T11:46:00+03:00", "Africa/Nairobi");

        staff.setZoneId("Asia/Chongqing");
        reservation.setStaff(staff);
        anotherReservation.setStaff(staff);
        staff.getReservations().add(reservation);
        staff.getReservations().add(anotherReservation);
        staffRepository.save(staff);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/staffs/1/reservations"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(jsonPath("$[0].username").value("Sofia"))
                .andExpect(jsonPath("$[0].startTime.client.startTime").value("2019-08-20T11:46:00+03:00"))
                .andExpect(jsonPath("$[0].startTime.client.zoneId").value("Africa/Nairobi"))
                .andExpect(jsonPath("$[0].startTime.staff.startTime").value("2019-08-20T16:46:00+08:00"))
                .andExpect(jsonPath("$[0].startTime.staff.zoneId").value("Asia/Chongqing"))
                .andExpect(jsonPath("$[1].username").value("Jack"))
                .andExpect(jsonPath("$[1].startTime.client.startTime").value("2019-08-21T11:46:00+08:00"))
                .andExpect(jsonPath("$[1].startTime.client.zoneId").value("Asia/Chongqing"))
                .andExpect(jsonPath("$[1].startTime.staff.startTime").value("2019-08-21T11:46:00+08:00"))
                .andExpect(jsonPath("$[1].startTime.staff.zoneId").value("Asia/Chongqing"));
    }

    @Test
    void should_get_status_409_when_staff_unqualified() throws Exception {
        Staff staff = new Staff("Rob", "Hall");
        String content = "{\n" +
                "  \"username\": \"Sofia\",\n" +
                "  \"companyName\": \"ThoughtWorks\",\n" +
                "  \"zoneId\": \"Africa/Nairobi\",\n" +
                "  \"startTime\": \"2019-08-20T11:46:00+03:00\",\n" +
                "  \"duration\": \"PT1H\"\n" +
                "}\n";

        staffRepository.save(staff);
        staffRepository.flush();

        mockMvc.perform(MockMvcRequestBuilders.post("/api/staffs/1/reservations")
                .contentType("application/json")
                .content(content))
                .andExpect(MockMvcResultMatchers.status().is(409))
                .andExpect(jsonPath("$.message").value("Rob Hall is not qualified."));
    }

    @Test
    void should_get_status_400_when_reservation_time_is_less_than_48h_from_now() throws Exception {
        Staff staff = new Staff("Rob", "Hall");
        String content = "{\n" +
                "  \"username\": \"Sofia\",\n" +
                "  \"companyName\": \"ThoughtWorks\",\n" +
                "  \"zoneId\": \"Africa/Nairobi\",\n" +
                "  \"startTime\": \"2019-10-14T11:46:00+03:00\",\n" +
                "  \"duration\": \"PT1H\"\n" +
                "}\n";

        staff.setZoneId("Asia/Chongqing");
        staffRepository.save(staff);
        staffRepository.flush();

        mockMvc.perform(MockMvcRequestBuilders.post("/api/staffs/1/reservations")
                .contentType("application/json")
                .content(content))
                .andExpect(MockMvcResultMatchers.status().is(400))
                .andExpect(jsonPath("$.message").value("Invalid time: the application is too close from now. The interval should be greater than 48 hours."));
    }

    @Test
    void should_get_status_409_when_reservation_time_is_out_of_work_time() throws Exception {
        Staff staff = new Staff("Rob", "Hall");
        String content = "{\n" +
                "  \"username\": \"Sofia\",\n" +
                "  \"companyName\": \"ThoughtWorks\",\n" +
                "  \"zoneId\": \"Africa/Nairobi\",\n" +
                "  \"startTime\": \"2019-12-14T19:46:00+03:00\",\n" +
                "  \"duration\": \"PT1H\"\n" +
                "}\n";

        staff.setZoneId("Asia/Chongqing");
        staffRepository.save(staff);
        staffRepository.flush();

        mockMvc.perform(MockMvcRequestBuilders.post("/api/staffs/1/reservations")
                .contentType("application/json")
                .content(content))
                .andExpect(MockMvcResultMatchers.status().is(409))
                .andExpect(jsonPath("$.message").value("You know, our staff has their own life."));
    }

    @Test
    void should_get_status_400_when_duration_is_greater_than_3() throws Exception {
        Staff staff = new Staff("Rob", "Hall");
        String content = "{\n" +
                "  \"username\": \"Sofia\",\n" +
                "  \"companyName\": \"ThoughtWorks\",\n" +
                "  \"zoneId\": \"Africa/Nairobi\",\n" +
                "  \"startTime\": \"2019-12-20T11:46:00+03:00\",\n" +
                "  \"duration\": \"PT4H\"\n" +
                "}\n";

        staff.setZoneId("Asia/Chongqing");
        staffRepository.save(staff);
        staffRepository.flush();

        mockMvc.perform(MockMvcRequestBuilders.post("/api/staffs/1/reservations")
                .contentType("application/json")
                .content(content))
                .andExpect(MockMvcResultMatchers.status().is(400))
                .andExpect(jsonPath("$.message").value("The application duration should be within 1-3 hours."));
    }

}
